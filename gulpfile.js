// ------------
// DEPENDENCIES
// ------------
var fs = require("fs");
var argv = require('yargs').argv;

var gulp = require("gulp");
var sass = require("gulp-sass");
var notify = require("gulp-notify");
var bulkSass = require('gulp-sass-bulk-import');
var gutil = require('gulp-util');
var clean = require('gulp-clean');
var taskListing = require('gulp-task-listing');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var merge = require('merge-stream');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');


// -------
// OPTIONS
// -------
var JS_DEST = "valhalla/static/js/";
var CSS_DEST = "valhalla/static/styles/";

var SASS_PATH = CSS_DEST + "main.scss";
var WATCH_FOLDER = CSS_DEST + "/**/*.scss";

var FILES_TO_CLEAN = [
  CSS_DEST + "**/*.css",
  CSS_DEST + "Vendor/**/*",
  JS_DEST + "Vendor/**/*"
];

var WATCH_TASKS = [
    "build-sass"
];

var VENDOR_FILES = [
    ['node_modules/flexiblegs-scss/flexiblegs-scss.scss', 'flexible-gs'],
    ['node_modules/jquery/dist/jquery.js'],
    ['node_modules/vue/dist/vue.js'],
    ['node_modules/bootstrap/dist/css/bootstrap.css'],
    ['node_modules/bootstrap/dist/js/bootstrap.js'],
];

var CSS_OPTIONS = {
    roundingPrecision: 5
};

var SASS_OPTIONS = {
    outputStyle: "compressed",
    precision: 5,
    remove: true
};

var PREFIXER_OPTIONS = {
    browsers: ['> 5% in BR'],
    cascade: false,
    flexbox: true
};

var CLEAN_OPTIONS = {
    read: false
};


// -------
// HELPERS
// -------
function error_msg(err) {
    /** Returns a custom error message for gulp-notify */

    var error_string = err.toString().replace(/(\r\n|\n|\r)/gm, "");

    var strIndex = function(string) {
        return error_string.indexOf(string);
    };

    var strSub = function(int_1, int_2) {
        return error_string.substring(int_1, int_2);
    };

    var pos = {
        message: strIndex("Message:") + 28,
        error: strIndex("Error:"),
        column: strIndex("column:"),
        line: strIndex("line:") + 6,
        formated: strIndex("formatted:"),
        original: strIndex("messageOriginal:") + 17,
        path: strIndex("relativePath:")
    };

    var msg = {
        error: strSub(pos.original, pos.path),
        line: strSub(pos.line, pos.column - 4),
        column: strSub(pos.column + 8, pos.formated - 4),
        file: strSub(pos.message, pos.error).split("_").splice(-1),
    };

    return {
        message: msg.error,
        title: msg.file + " @ " + msg.line + ":" + msg.column
    };
}


// -------
// Streams
// -------
function clean_project() {
  var streams = [];

  FILES_TO_CLEAN.forEach(function(item) {
      var stream = gulp.src(item, CLEAN_OPTIONS)
          .pipe(clean());

      streams.push(stream);
    });

  return merge(streams);
}


function sass_build() {
    /** Compiles and builds *.scss files */
    var scss_to_css = sass(SASS_OPTIONS).on("error", notify.onError(error_msg));

    gutil.log('Trying to compile css at', gutil.colors.cyan(CSS_DEST));

    return gulp.src(SASS_PATH)
        .pipe(bulkSass())
        .pipe(scss_to_css)
        .pipe(autoprefixer(PREFIXER_OPTIONS))
        .pipe(gulp.dest(CSS_DEST));
};


function watch_files() {
    /** Watch for files changes to run specific tasks */
    gutil.log('Watching', gutil.colors.cyan(WATCH_FOLDER));
    gutil.log('Press', gutil.colors.cyan('ctrl+c'), 'to end the watch process');

    gulp.watch(WATCH_FOLDER, WATCH_TASKS);
}


function copy_vendor() {
    var streams = [];

    VENDOR_FILES.forEach(function(item) {
        function rename_route(path) {
            if (item.length > 1) {
                path.basename = item[1];
            }

            if (path.extname == '.css' || path.extname == '.scss') {
                path.dirname = CSS_DEST + 'vendor'
                gutil.log(path.dirname);
            }

            if (path.extname == '.js') {
                path.dirname = JS_DEST + 'vendor'
                gutil.log(path.dirname);
            }
        }

        var stream = gulp.src(item[0])
            .pipe(rename(rename_route))
            .pipe(gulp.dest('.'));

        streams.push(stream);
    });

    return merge(streams);
}


function minify_vendor_css() {
    if (argv.mode != "Debug") {
        return gulp.src(CSS_DEST + 'vendor/**/*.css')
            .pipe(cleanCSS(CSS_OPTIONS))
            .pipe(gulp.dest(CSS_DEST + 'vendor'));
    }
}


function uglify_vendor_js() {
    if (argv.mode != "Debug") {
        return gulp.src(JS_DEST + 'vendor/**/*.js')
            .pipe(uglify())
            .pipe(gulp.dest(JS_DEST + 'vendor'));
    }
}


// -----
// Tasks
// -----
gulp.task('default', taskListing.withFilters(null, 'default'));
gulp.task('help', taskListing.withFilters(null, 'default'));
gulp.task('watch', watch_files);
gulp.task('clean', clean_project);
gulp.task('build-sass', sass_build);
gulp.task('copy-vendor', copy_vendor);
gulp.task('minify-vendor-css', ['copy-vendor'], minify_vendor_css);
gulp.task('uglify-vendor-js', ['copy-vendor'], uglify_vendor_js);

gulp.task('build', ['copy-vendor', 'minify-vendor-css', 'uglify-vendor-js', 'build-sass']);
