from flask import Blueprint, jsonify

blueprint = Blueprint('api', __name__)


@blueprint.route('/get-books')
def get_books():
	library  = [
		{
			'name': 'Book of Dooooooom',
			'author': 'Jorge'
		},
		{
			'name': 'Book of Knowledge',
			'author': 'Maria'
		}
	]

	return jsonify(library)


# @blueprint.route('/save-book', methods=['POST'])
# def save_book():
# 	result = {
# 		'status': OK
# 	}

# 	return jsonify(result)