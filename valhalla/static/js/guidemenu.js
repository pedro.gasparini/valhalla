var example1 = new Vue({
    delimiters: ['[', ']'],
    el: '[data-js-my-index]',
    data: {
        menu: [{
                name: 'Introdução',
                href: '#guide-overview'
            }, {
                name: 'Navegação',
                href: '#guide-navigation'
            }, {
                name: 'Cores',
                href: '#guide-colors'
            }, {
                name: 'Responsividade',
                href: '#guide-responsivity'
            }, {
                name: 'Ícones',
                href: '#guide-icons'
            }, {
                name: 'Botões',
                href: '#guide-buttons'
            }, {
                name: 'Tipografia',
                href: '#guide-typography'
            },

        ]
    }
});
