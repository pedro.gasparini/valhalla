from flask import Flask
from views import blueprint as views_blueprint
from api import blueprint as api_blueprint
import jinja2

app = Flask(__name__, instance_relative_config=True)

app.config.from_object('config')
# app.config.from_pyfile('config.py')

def include_file(name):
    return jinja2.Markup(loader.get_source(env, name)[0])

loader = jinja2.PackageLoader(__name__, 'static')
env = jinja2.Environment(loader=loader)
app.jinja_env.globals['include_file'] = include_file

app.register_blueprint(views_blueprint)
app.register_blueprint(api_blueprint)
