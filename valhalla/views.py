from flask import Blueprint, render_template, redirect, url_for

blueprint = Blueprint('views', __name__)


@blueprint.route('/')
@blueprint.route('/index')
def index():
	return render_template('index.html')


@blueprint.route('/guide')
def guide():
	return render_template('guide.html')

@blueprint.route('/cases')
def cases():
	return render_template('cases.html')

@blueprint.route('/experience')
def experience():
	return render_template('experience.html')

@blueprint.route('/research')
def research():
	return render_template('research.html')

@blueprint.route('/experience-inner')
def experienceinner():
	return render_template('experience-inner.html')

@blueprint.route('/testimonials')
def testimonials():
	return render_template('testimonials.html')

@blueprint.route('/contact')
def contact():
	return render_template('contact.html')


@blueprint.app_errorhandler(404)
def page_not_found(e):
    return render_template('not-found.html'), 404
